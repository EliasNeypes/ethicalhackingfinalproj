Run:


To see options: `./flask_app.py -h`


Sample script: `./flask_app.py -w 100 -d 3 -u http://dvwa.ethicalhacking.fun --cookie 'PHPSESSID=5mungig3cs9k6nrfm1jodahsf2; security=low' -i 3.17.144.236`


Note: to pass values with spaces in command line, use quotes (ex: cookie field)


Todo:
-	Remove hardcoded values
	-	exfiltration IP address in payload
	-	web page to crawl
	-	crawler parameters (depth, width, blacklist)
	-	crawler authentication (just cookie should probably be okay)
-	Figure out what dependencies are needed by xsser
