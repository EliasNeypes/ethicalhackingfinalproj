#!/bin/bash

./flask_app.py \
	-i '3.17.144.236' \
	-u 'https://dvwa.ethicalhacking.fun' \
	-Cb 'logout\.php' \
	-Cb 'setup\.php' \
	-Cb 'vulnerabilities\.php' \
	-Cb 'security\.php' \
	--cookie 'PHPSESSID=5q3pus659oju1cj1pt880hb722; security=low' \
